// Fill out your copyright notice in the Description page of Project Settings.

#include "SCharacter.h"
#include "..\Public\SCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "CoopGame.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Net/UnrealNetwork.h"
#include "SHealthComponent.h"
#include "SWeapon.h"

// Sets default values
ASCharacter::ASCharacter()
{
    PrimaryActorTick.bCanEverTick = true;
    SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
    SpringArmComp->bUsePawnControlRotation = true;
    SpringArmComp->SetupAttachment(RootComponent);

    GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;

    GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

    HealthComp = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComp"));

    CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
    // CameraComp->bUsePawnControlRotation = true;
    CameraComp->SetupAttachment(SpringArmComp);

    zoomedFOV = 65.f;
    zoomedSpeed = 20.f;
    WeaponAttachSocketName = "WeaponSocket";
}

void ASCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    PlayerInputComponent->BindAxis("MoveForward", this, &ASCharacter::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &ASCharacter::MoveRight);

    PlayerInputComponent->BindAxis("LookUp", this, &ASCharacter::AddControllerPitchInput);
    PlayerInputComponent->BindAxis("Turn", this, &ASCharacter::AddControllerYawInput);

    PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ASCharacter::BeginCrouch);
    PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ASCharacter::EndCrouch);

    PlayerInputComponent->BindAction("Zoom", IE_Pressed, this, &ASCharacter::BeginZoom);
    PlayerInputComponent->BindAction("Zoom", IE_Released, this, &ASCharacter::EndZoom);

    PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASCharacter::StartFire);
    PlayerInputComponent->BindAction("Fire", IE_Released, this, &ASCharacter::StopFire);
    // PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASCharacter::Fire);

    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ASCharacter::Jump);
}

void ASCharacter::BeginPlay()
{
    Super::BeginPlay();
    defaultFOV = CameraComp->FieldOfView;
    if (HealthComp)
        HealthComp->OnHealthChanged.AddDynamic(this, &ASCharacter::OnHealthChanged);

    if (GetLocalRole() == ENetRole::ROLE_Authority) {
        FActorSpawnParameters spawnParams;
        spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
        currentWeapon = GetWorld()->SpawnActor<ASWeapon>(StarterWeaponClass, FVector::ZeroVector,
            FRotator::ZeroRotator, spawnParams);
        if (currentWeapon) {
            currentWeapon->SetOwner(this);
            currentWeapon->AttachToComponent(GetMesh(),
                FAttachmentTransformRules::SnapToTargetNotIncludingScale, "WeaponSocket");
        }
    }
}

void ASCharacter::OnHealthChanged(USHealthComponent* HealthComp1, float Health, float HealthDelta,
    const class UDamageType* DamageType, class AController* instigatedBy, AActor* damageCauser)
{
    if (Health <= .0f && !bDied) {

        GetMovementComponent()->StopMovementImmediately();
        GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
        DetachFromControllerPendingDestroy();

        SetLifeSpan(10.f);
        bDied = true;
    }
    if (currentWeapon)
        currentWeapon->addText(FString::Printf(TEXT("Health: %f"), Health));
}
#pragma region Input
void ASCharacter::MoveForward(float val)
{
    AddMovementInput(GetActorForwardVector() * val);
}

void ASCharacter::MoveRight(float val)
{
    AddMovementInput(GetActorRightVector() * val);
}

void ASCharacter::BeginCrouch()
{
    Crouch();
}

void ASCharacter::EndCrouch()
{
    UnCrouch();
}

void ASCharacter::BeginZoom()
{
    wantsToZoom = true;
}

void ASCharacter::EndZoom()
{
    wantsToZoom = false;
}

void ASCharacter::Jump()
{
    Super::Jump();
}

void ASCharacter::StartFire()
{
    if (currentWeapon)
        currentWeapon->StartFire();
}

void ASCharacter::StopFire()
{
    if (currentWeapon)
        currentWeapon->StopFire();
}
#pragma endregion

void ASCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    float TargetFOV = wantsToZoom ? zoomedFOV : defaultFOV;
    float NewFOV = FMath::FInterpTo(CameraComp->FieldOfView, TargetFOV, DeltaTime, zoomedSpeed);
    CameraComp->SetFieldOfView(NewFOV);
}

FVector ASCharacter::GetPawnViewLocation() const
{
    if (CameraComp)
        return CameraComp->GetComponentLocation();

    return Super::GetPawnViewLocation();
}

void ASCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ASCharacter, currentWeapon);
    DOREPLIFETIME(ASCharacter, bDied);
}
