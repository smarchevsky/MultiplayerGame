// Fill out your copyright notice in the Description page of Project Settings.

#include "SWeapon.h"
#include "..\Public\SWeapon.h"

#include "Components/SkeletalMeshComponent.h"
#include "Components/TextRenderComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TimerManager.h"

#include "CoopGame.h"

static void log_screen(const FString& str, float time = 2.f)
{
    if (GEngine)
        GEngine->AddOnScreenDebugMessage(-1, time, FColor::White, str);
}

static int32 DebugWeaponDrawing = 0;
FAutoConsoleVariableRef CVARDebugWeaponDrawing(
    TEXT("COOP.DebugWeapons"),
    DebugWeaponDrawing,
    TEXT("Draw debug lines for weapons"),
    ECVF_Cheat);

ASWeapon::ASWeapon()
{
    MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
    m_textComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRenderComp"));
    RootComponent = MeshComp;
    m_textComponent->SetupAttachment(RootComponent);
    m_textComponent->SetIsReplicated(true);

    PrimaryActorTick.bCanEverTick = true;

    MuzzleSocketName = "MuzzleSocket";
    TracerTargetName = "Target";
    BaseDamage = 20.f;
    RateOfFire = 600;
    SetReplicates(true);

    //NetUpdateFrequency = 66.f;
    //MinNetUpdateFrequency = 33.f;
}

void ASWeapon::BeginPlay()
{
    Super::BeginPlay();
    m_textComponent->SetText(FText());
    TimeBetweenShots = 60.f / RateOfFire;
}

void ASWeapon::StartFire()
{
    float FirstDelay = FMath::Max(lastFireTime + TimeBetweenShots - GetWorld()->TimeSeconds, 0.f);

    GetWorldTimerManager().SetTimer(timerHandle_TimeBetweenShots, this, &ASWeapon::Fire, TimeBetweenShots, true, FirstDelay);
}

void ASWeapon::StopFire()
{
    GetWorldTimerManager().ClearTimer(timerHandle_TimeBetweenShots);
}

void ASWeapon::Fire()
{
    /*FString text;
    if (auto own = GetOwner())
        text += (own->GetLocalRole() == ROLE_Authority) ? TEXT("OwnServer ") : TEXT("OwnClient ");
    text += (GetLocalRole() == ROLE_Authority) ? TEXT("WeapServer ") : TEXT("WeapClient ");
    addText(text);*/

    if (GetLocalRole() < ENetRole::ROLE_Authority) { // if client
        addText("LocalFire On Client");
        ServerFire();
        // return;
    } else
        addText("LocalFire On Server");

    AActor* myOwner = GetOwner();
    if (myOwner) {

        FVector eyeLoc;
        FRotator eyeRotation;
        myOwner->GetActorEyesViewPoint(eyeLoc, eyeRotation);
        FVector traceEnd = eyeLoc + (eyeRotation.Vector() * 10000.f);
        FVector ShotDirection = eyeRotation.Vector();

        FCollisionQueryParams queryParams;
        queryParams.AddIgnoredActor(myOwner);
        queryParams.AddIgnoredActor(this);
        queryParams.bTraceComplex = true;
        queryParams.bReturnPhysicalMaterial = true;

        FHitResult Hit;
        EPhysicalSurface physSurface = EPhysicalSurface::SurfaceType_Max;
        if (GetWorld()->LineTraceSingleByChannel(
                Hit, eyeLoc, traceEnd, COLLISION_WEAPON, queryParams)) {
            traceEnd = Hit.ImpactPoint;

            AActor* hitActor = Hit.GetActor();
            physSurface = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

            float actualDamage = BaseDamage;
            if (physSurface == SURFACE_FLESHVULNERABLE) {
                actualDamage *= 4.0f;
            }
            UGameplayStatics::ApplyPointDamage(hitActor, actualDamage, ShotDirection, Hit,
                myOwner->GetInstigatorController(), this, DamageType);
        }
        bool serv = myOwner->GetLocalRole() == ENetRole::ROLE_Authority;
        if (DebugWeaponDrawing)
            DrawDebugLine(GetWorld(), eyeLoc, traceEnd, serv ? FColor::White : FColor::Cyan, false, 1.f, 0, 2.f);

        PlayFireEffects(physSurface, traceEnd);

        if (GetLocalRole() == ENetRole::ROLE_Authority) {
            hitTrace.traceTo = traceEnd;
            hitTrace.physSurface = physSurface;
        }

        lastFireTime = GetWorld()->TimeSeconds;
    }
}

void ASWeapon::OnRep_hitTrace()
{
    // CLIENT ONLY
    addText("PlayFireEffects");
    PlayFireEffects(hitTrace.physSurface, hitTrace.traceTo);
}

void ASWeapon::ServerFire_Implementation()
{
    // SERVER ONLY
    addText("Client to Server");
    Fire();
}

bool ASWeapon::ServerFire_Validate() { return true; }

void ASWeapon::PlayFireEffects(EPhysicalSurface surface, FVector traceEnd)
{
    if (surface != SurfaceType_Max) {
        UParticleSystem* SelectedEffect = nullptr;
        switch (surface) {
        case SURFACE_FLESHDEFAULT:
        case SURFACE_FLESHVULNERABLE:
            SelectedEffect = FleshImpactEffect;
            break;
        default:
            SelectedEffect = DefaultImpactEffect;
            break;
        }
        auto muzzleLoc = MeshComp->GetSocketLocation(MuzzleSocketName);
        FVector shotDir = (traceEnd - muzzleLoc).GetSafeNormal();
        if (SelectedEffect)
            UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedEffect, traceEnd, shotDir.Rotation());
        if (MuzzleEffect)
            UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);
    }

    if (TracerEffect) {
        FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
        auto tracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MuzzleLocation);

        if (tracerComp)
            tracerComp->SetVectorParameter(TracerTargetName, traceEnd);
    }
    APawn* owner = Cast<APawn>(GetOwner());
    if (owner) {
        APlayerController* PC = owner->GetController<APlayerController>();
        if (PC && FireCamShake) {
            PC->ClientStartCameraShake(FireCamShake);
        }
    }
}

void ASWeapon::addText(FString str, float time)
{
    m_stringBatch.addString(str, time);
    m_stringBatch.dirty = true;
}

void ASWeapon::updateTextMsg(float dt)
{

    auto& strings = m_stringBatch.visibleStrings;
    for (int i = 0; i < strings.Num(); ++i) {
        strings[i].currentTime -= dt;
        if (strings[i].currentTime < 0.f) {
            strings.RemoveAt(i);
            i = i < 0 ? 0 : i--;
            m_stringBatch.dirty = true;
        }
    }
    if (m_stringBatch.dirty) {
        FString text;
        for (const auto& s : strings)
            text.Append(s.str + "\n");

        m_textComponent->SetText(FText::FromString(text));
        m_stringBatch.dirty = false;
    }
}

void ASWeapon::Tick(float dt)
{
    Super::Tick(dt);
    updateTextMsg(dt);
}

void ASWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME_CONDITION(ASWeapon, hitTrace, COND_SkipOwner);
}
