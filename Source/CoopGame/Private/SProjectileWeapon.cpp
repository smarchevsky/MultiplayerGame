// Fill out your copyright notice in the Description page of Project Settings.

#include "SProjectileWeapon.h"

void ASProjectileWeapon::Fire()
{
    AActor* myOwner = GetOwner();
    if (myOwner && ProjectileClass) {
        FVector eyeLoc;
        FRotator eyeRotation;
        myOwner->GetActorEyesViewPoint(eyeLoc, eyeRotation);

        FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
        //FRotator MuzzleRoatation = MeshComp->GetSocketRotation(MuzzleSocketName);

        FActorSpawnParameters spawnParams;
        spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
        GetWorld()->SpawnActor<AActor>(ProjectileClass, MuzzleLocation, eyeRotation, spawnParams);
    }
}
