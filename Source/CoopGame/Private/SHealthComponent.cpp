// Fill out your copyright notice in the Description page of Project Settings.

#include "SHealthComponent.h"
#include "Net/UnrealNetwork.h"
// Sets default values for this component's properties
USHealthComponent::USHealthComponent()
{
    DefaultHealth = 100;
}

// Called when the game starts
void USHealthComponent::BeginPlay()
{
    Super::BeginPlay();
    SetIsReplicated(true);
    if (GetOwnerRole() == ROLE_Authority) {
        if (auto myOwner = GetOwner())
            myOwner->OnTakeAnyDamage.AddDynamic(
                this, &USHealthComponent::HandleTakeAnyDamage);
    }

    Health = DefaultHealth;
}

void USHealthComponent::HandleTakeAnyDamage(AActor* damageActor, float damage,
    const UDamageType* DamageType, AController* instigatedBy, AActor* damageCauser)
{
    if (damage <= 0.f)
        return;

    Health = FMath::Clamp(Health - damage, 0.f, DefaultHealth);
    UE_LOG(LogTemp, Log, TEXT("Health changed %s"), *FString::SanitizeFloat(Health));

    OnHealthChanged.Broadcast(this, Health, damage, DamageType, instigatedBy, damageCauser);
}

void USHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(USHealthComponent, Health);
    //DOREPLIFETIME(ASWeapon, hitTrace);
}
