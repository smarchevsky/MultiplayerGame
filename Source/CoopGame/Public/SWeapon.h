// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "SWeapon.generated.h"

class USkeletalMeshComponent;
class UDamageType;
class UParticleSystem;

USTRUCT()
struct FHitScanTrace {
    GENERATED_BODY()
public:
    UPROPERTY()
    TEnumAsByte<EPhysicalSurface> physSurface;
    UPROPERTY()
    FVector_NetQuantize traceTo;
};

UCLASS()
class COOPGAME_API ASWeapon : public AActor {
    GENERATED_BODY()

public:
    ASWeapon();

protected:
    virtual void BeginPlay() override;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
    USkeletalMeshComponent* MeshComp;

    void PlayFireEffects(EPhysicalSurface surface,
        FVector traceEnd);

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    TSubclassOf<UDamageType> DamageType;

    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    FName MuzzleSocketName;

    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    FName TracerTargetName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    UParticleSystem* MuzzleEffect;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    UParticleSystem* DefaultImpactEffect;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    UParticleSystem* FleshImpactEffect;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    UParticleSystem* TracerEffect;

    UPROPERTY(EditDefaultsOnly, Category = "Weapon")
    class UTextRenderComponent* m_textComponent;

    UPROPERTY(EditDefaultsOnly, Category = "Weapon")
    TSubclassOf<UMatineeCameraShake> FireCamShake;

    UPROPERTY(EditDefaultsOnly, Category = "Weapon")
    float BaseDamage;

    virtual void Fire();

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerFire();

    FTimerHandle timerHandle_TimeBetweenShots;

    float lastFireTime;

    /*RPM bullter for minute*/
    UPROPERTY(EditDefaultsOnly, Category = "Weapon")
    float RateOfFire;

    // Dreived from rate of fire
    float TimeBetweenShots;

    UPROPERTY(ReplicatedUsing = OnRep_hitTrace)
    FHitScanTrace hitTrace;

    UFUNCTION()
    void OnRep_hitTrace();

    /////////////////////////// DEBUG_TEXT ///////////////
    struct StringBatch {
        struct Strings {
            FString str;
            float currentTime = {};
        };
        void addString(FString str, float time = 1.f)
        {
            visibleStrings.Add({ str, time });
        }
        TArray<Strings> visibleStrings;
        bool dirty = false;
    } m_stringBatch;
    void updateTextMsg(float dt);
    void Tick(float dt) override;
    //////////////////////////////////////////////////////

public:
    void addText(FString str, float time = 1.f);

    virtual void StartFire();

    virtual void StopFire();
};
