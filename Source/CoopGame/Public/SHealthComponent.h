// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"

#include "SHealthComponent.generated.h"

// on health changed event
DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(FOnHealthChangeSignature, USHealthComponent*, HealthComp, float, Health, float, HealthDelta,
    const class UDamageType*, DamageType, class AController*, instigatedBy, AActor*, damageCauser);

UCLASS(ClassGroup = (COOP), meta = (BlueprintSpawnableComponent))
class COOPGAME_API USHealthComponent : public UActorComponent {
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    USHealthComponent();

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

    UPROPERTY(Replicated, BlueprintReadOnly, Category = "HealthComponent")
    float Health;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HealthComponent")
    float DefaultHealth;

    UFUNCTION()
    void HandleTakeAnyDamage(AActor* damageActor, float damage,
        const class UDamageType* DamageType, class AController* instigatedBy, AActor* damageCauser);

public:
    UPROPERTY(BlueprintAssignable, Category = "Events")
    FOnHealthChangeSignature OnHealthChanged;
};
