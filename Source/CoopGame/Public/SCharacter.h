// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "SCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class ASWeapon;
class USHealthComponent;

UCLASS()
class COOPGAME_API ASCharacter : public ACharacter {
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    ASCharacter();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    void MoveForward(float val);

    void MoveRight(float val);

    void BeginCrouch();
    void EndCrouch();

    void BeginZoom();
    void EndZoom();

    void StartFire();
    void StopFire();

    void Jump() override;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
    UCameraComponent* CameraComp;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
    USpringArmComponent* SpringArmComp;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
    USHealthComponent* HealthComp;

    bool wantsToZoom;

    UPROPERTY(EditDefaultsOnly, Category = "Player")
    float zoomedFOV;

    UPROPERTY(EditDefaultsOnly,
        Category = "Player",
        meta = (ClampMin = 0.1, ClampMax = 100.0))
    float zoomedSpeed;

    float defaultFOV;

    UPROPERTY(Replicated)
    ASWeapon* currentWeapon;

    UPROPERTY(EditDefaultsOnly, Category = "Player")
    TSubclassOf<ASWeapon> StarterWeaponClass;

    UPROPERTY(VisibleDefaultsOnly, Category = "Player")
    FName WeaponAttachSocketName;

    UFUNCTION()
    void OnHealthChanged(USHealthComponent* healthComp1,
        float Health,
        float HealthDelta,
        const class UDamageType* DamageType,
        class AController* instigatedBy,
        AActor* damageCauser);

    UPROPERTY(Replicated, BlueprintReadOnly, Category = "Playerl")
    bool bDied;

public:
    virtual void Tick(float DeltaTime) override;

    virtual FVector GetPawnViewLocation() const override;

    virtual void SetupPlayerInputComponent(
        class UInputComponent* PlayerInputComponent) override;
};
